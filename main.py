import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

G = nx.Graph()
G.graph["treasure"] = "diamond treasure hunt"

G.add_edges_from([
    ("9", "5"),
    ("9", "14"),
    ("5", "3"),
    ("5", "6"),
    ("3", "1"),
    ("3", "4"),
    ("14", "11"),
    ("14", "17"),
    ("11", "10"),
    ("11", "12"),
    ("17", "15"),
    ("17", "19")
])

pos = {
    "9":(22.64, 38.39),
    "5":(13.5, 30.4),
    "3":(5.77, 22.42),
    "1":(-2.08, 19.86),
    "4":(0.35, 14.34),
    "6":(15.80, 24),
    "14":(27, 31.24),
    "11":(21.86, 21.68),
    "17":(30.37, 23.74),
    "10":(14.89,14.33),
    "12":(24.58, 10.92),
    "15":(28.30, 15.84),
    "19":(34.45, 14.07)
}


def draw_graph(G, pos, collected_diamonds, order):
    plt.clf()
    node_colors = ["blue" 
                   if node in collected_diamonds else "green" if node == order[len(collected_diamonds)] 
                   else "red" for node in G.nodes]
    
    diamond_labels = {node: 'Diamond found!' if node in collected_diamonds else '' for node in G.nodes}
    diamond_pos = {node: (coords[0], coords[1] - 0.02) for node, coords in pos.items()}
    
    nx.draw(G, pos=pos, with_labels=False, node_color=node_colors, node_size=2000,
            edge_color="yellow", width=5)
    nx.draw_networkx_labels(G, pos=pos, font_color="white",
                            font_size=20, font_family="Arial", font_weight="bold")
    nx.draw_networkx_labels(G, pos=diamond_pos, labels=diamond_labels, font_color="gold",
                            font_size=20, font_family="Arial", font_weight="bold")
    
    plt.text(0.05, 0.95, f"Diamonds Collected: {len(collected_diamonds)} diamonds",
             transform=plt.gca().transAxes, fontsize=16, verticalalignment='top')
    

    
    
    plt.margins(0.2)

def onclick(event):
    global G, pos, visited, collected_diamonds
    min_distance = float("inf")
    clicked_node = None
    for node, coords in pos.items():
        dist = np.sqrt((coords[0] - event.xdata) ** 2 + (coords[1] - event.ydata) ** 2)
        if dist < min_distance:
            min_distance = dist
            clicked_node = node

    if clicked_node and clicked_node == order[len(collected_diamonds)] and clicked_node not in collected_diamonds:
        collected_diamonds.add(clicked_node)
        print(f"Diamond collected at {clicked_node}")
        draw_graph(G, pos, collected_diamonds, order)
        plt.draw()

order = ["9", "5", "3", "1", "4", "6", "14", "11", "10", "12", "17", "15", "19"]
visited = set()
collected_diamonds = set()

plt.figure(figsize=(12, 8))
draw_graph(G, pos, collected_diamonds, order)
plt.connect('button_press_event', onclick)
plt.show()


'''
#adjacent list: 

adjacency list = {
    "9": ["5", "14"]
    "5": ["9", "3", "6"]
    "3": ["5", "1", "4"]
    "1": ["3"]
    "4": ["3"]
    "6": ["5"]
    "14": ["9", "11", "17"]
    "11": ["14", "10", "12"]
    "17": ["14", "15", "19"]
    "10": ["11"]
    "12": ["11"]
    "15": ["17"]
    "19": ["17"]
}


//////////////////////////////////////////////////

PSEUDO CODE - PRE ORDER

1. start at 9 (root)
2. visit 5
3. visit 3
4. visit 1
5. visit 4
6. visit 6
7. visit 14
8. visit 11
9. visit 10
10. visit 12
11. visit 17
12. visit 15
13. visit 19


'''
